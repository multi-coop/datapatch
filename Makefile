
# DEV commands
.PHONY: install-front 
install-front:
	cd frontend/src && npm install

.PHONY: run-front-dev
run-front-dev:
	cd frontend/src && npm run dev


.PHONY: install-back 
install-back:
	cd backend && pipenv install

.PHONY: run-back
run-back:
	cd backend && pipenv run uvicorn app.main:app --reload

.PHONY: run-db
run-db:
	docker-compose up -d db
	
.PHONY: run-back-with-docker
run-back-with-docker:
	docker-compose up -d backend