# DATA-PATCH

> https://wiki.resilience-territoire.ademe.fr/wiki/Data_Patch

Data Patch est un logiciel open source en cours de développement proposant une solution indépendante et libre au problème de partage de données et de contribution ouverte.

## Prérequis

### Backend
Avec l'utilisation de docker
- docker + docker-compose

Sans docker
- python 3.8 (avec pyenv par exemple)
- pipenv

### Frontend
- nvm + node 14.16.1
## installation

Créer 2 fichiers `.env` à partir des templates :
- `./example.env` > `./.env`
- `./frontend/src/example.env` > `./frontend/src/.env`

Ne pas oublier de configurer un serveur SMTP qui fonctionne (envoi d'un email à l'inscription)
 
Un fichier `Makefile` permet de lancer les commandes principales:
- `make install-front`
- `make install-back`

## dev
Frontend :
- `make run-front-dev`
Backend sans docker :
- lancer un service postgres
- `make run-back`
Backend avec docker :
- `make run-back-with-docker`
